import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Pattern;

public class SubnetCal extends JFrame {
    private JTextField ipAddressField;
    private JTextField subnetMaskField;
    private JButton calculateButton;
    private JLabel resultLabel;

    public SubnetCal() {
        setTitle("Subnet Calculator");
        setSize(400, 150);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout());

        ipAddressField = new JTextField(15);
        subnetMaskField = new JTextField(15);
        calculateButton = new JButton("Calculate");
        resultLabel = new JLabel("");

        calculateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculateSubnet();
            }
        });

        add(new JLabel("IP Address:"));
        add(ipAddressField);
        add(new JLabel("Subnet Mask:"));
        add(subnetMaskField);
        add(calculateButton);
        add(resultLabel);
    }

    private void calculateSubnet() {
        String ip = ipAddressField.getText();
        String subnetMask = subnetMaskField.getText();

        if (!isValidIP(ip) || !isValidSubnetMask(subnetMask)) {
            resultLabel.setText("Invalid input. Please enter valid IP and subnet mask.");
            return;
        }

        try {
            InetAddress inetAddress = InetAddress.getByName(ip);
            String hostAddress = inetAddress.getHostAddress();
            resultLabel.setText("IP Address: " + hostAddress);
        } catch (UnknownHostException e) {
            resultLabel.setText("Invalid IP address.");
        }
    }

    private boolean isValidIP(String ip) {
        // Use regular expression to validate IP address
        String ipPattern = "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
        return Pattern.matches(ipPattern, ip);
    }

    private boolean isValidSubnetMask(String subnetMask) {
        // Use regular expression to validate subnet mask
        String subnetMaskPattern = "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
        return Pattern.matches(subnetMaskPattern, subnetMask);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                SubnetCal calculator = new SubnetCal();
                calculator.setVisible(true);
            }
        });
    }
}
